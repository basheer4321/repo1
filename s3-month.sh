#!/bin/bash
#
# Description: The following script takes AMI of the SFTP server if 
#              the flag file exists in the S3 bucket.
#
# Run Information: This script should be run once in a day through crontab.
#
# Error Log: Any errors or output associated with the script can be found in /path/to/script/ami.log
#
if [ -z "$1" ] || [ -z "$2" ];then
    echo -e "\n Please execute the script as follows \n\n ./`basename "$0"` <primary_region> <dr_region> \n"
    exit 1
fi

script_log="./ami.log"
#date=$(date +"%Y%m%d")
date=$(date +"%Y-%m-%d_%H_%M_%S")
primary_region="$1"
dr_region="$2"
s3_bucket="mytest1212"


aws s3 ls s3://$s3_bucket/sdpamiid.txt 

if [[ $? -ne 0 ]]; then
  echo "File does not exist"
  exit 1
fi
    instance_id=`aws ec2 describe-instances --filters "Name=tag-value,Values=SFTP_server" --output text --query 'Reservations[].Instances[].InstanceId'`
    echo -e "Starting the AMI creation: SFTP_AMI_$date\n" >> $script_log
    created_ami=`aws ec2 create-image --instance-id $instance_id --name "SFTP_AMI_$date" --no-reboot --output text --description "AMI taking after changes found in SFTP server at $date"`

    #Tag is created for the new AMI, which will used during retention policy script
    aws ec2 create-tags --resources $created_ami --tags Key=ami,Value=sftp_replica_ami --region $primary_region

    #Copying the new AMI into DR region
    replicated_ami_id=`aws ec2 copy-image --source-image-id $created_ami --source-region $primary_region --region $dr_region --name "SFTP_AMI_$date" --output text`

    #Creating tag for the AMI in DR region, which will be used in retention policy script
    aws ec2 create-tags --resources $replicated_ami_id --tags Key=ami,Value=sftp_replica_ami --region $dr_region


    sleep 2m

    #Check AMI status
    primary_ami_status=`aws ec2 describe-images --image-ids $created_ami --output text --query 'Images[*].State' --region $primary_region`
    dr_ami_status=`aws ec2 describe-images --image-ids $replicated_ami_id --output text --query 'Images[*].State' --region $dr_region`
    if [ $primary_ami_status == "failed" ]; then
        echo "SFTP AMI $created_ami creation in primary_region($primary_region) failed at $date" >> $script_log
    else
        echo "SFTP AMI creation $created_ami in primary_region($primary_region) successfull at $date" >> $script_log
    fi

    if [ $dr_ami_status == "failed" ]; then
        echo "SFTP AMI $created_ami creation in DR region($dr_region) failed at $date" >> $script_log
    else
        echo "SFTP AMI $created_ami creation in DR region($dr_region) successfull at $date" >> $script_log
    fi

aws s3 rm s3://$s3_bucket/sdpamiid.txt
